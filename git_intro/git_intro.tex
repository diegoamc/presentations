\documentclass{beamer}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage[brazil]{babel}
\usepackage{graphicx}

\usepackage{hyperref}

\usetheme{JuanLesPins}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{caption}[numbered]
\setbeamertemplate{footline}[page number]
\usecolortheme{beaver}

\title{\textbf{An Introduction to Git}}

\author{Diego A. M. Camarinha \and Rafael R. Manzo}

\date{04/04/2018}

\begin{document}

\maketitle

%\begin{frame}{Contents}
%  \tableofcontents
%\end{frame}

\section{Context}

\begin{frame}{What is Version Control?}
  \begin{block}{Definition}
    A system that records changes to a file or a set of files over time so that
    you can recall specific changes later.
  \end{block}

  A file can be:
  \begin{itemize}
    \item A source code file
    \item An image
    \item A video
    \item Any kind of file, really
  \end{itemize}
\end{frame}

\begin{frame}{Why do people use Version Control?}
  \begin{block}{The short answer}
    Because if you screw things up or lose files, you can easily recover.
  \end{block}

  More specifically, you can:
  \begin{itemize}
    \item Revert selected files to a previous state
    \item Revert an entire project to a previous state
    \item Compare changes
    \item See who last modified a file
  \end{itemize}

  All of this can be achieved by copying files to another directory or renaming
  file names.
\end{frame}

\begin{frame}{Local Version Control Systems}
  \begin{figure}
    \centering
    \includegraphics[scale=0.33]{figures/lvcs.png}
  \end{figure}
  Example: RCS
\end{frame}

\begin{frame}{Centralized Version Control Systems}
  \begin{figure}
    \centering
    \includegraphics[scale=0.32]{figures/cvcs.png}
  \end{figure}
  Examples: CVS, Subversion, Perforce
\end{frame}

\begin{frame}{Distributed Version Control Systems}
  \begin{figure}
    \centering
    \includegraphics[scale=0.3]{figures/dvcs.png}
  \end{figure}
  Examples: \textbf{Git}, Mercurial, Bazaar
\end{frame}

\section{Git}

\begin{frame}{Git Basics}
  \framesubtitle{Snapshots, Not Differences}
  \centering
  Storing data as changes to a base version of each file
  \includegraphics[scale=0.3]{figures/differences.png}

  Storing data as snapshots of the project over time
  \includegraphics[scale=0.3]{figures/snapshots.png}
\end{frame}

\begin{frame}{Git Basics}
  \begin{itemize}
    \item Nearly every operation is local
    \begin{itemize}
      \item Faster operations
      \item Work offline
    \end{itemize}
    \item Integrity
    \begin{itemize}
      \item Git knows if a information is lost or if a file is corrupted
      \item Stores hash values, not file names
    \end{itemize}
    \item Generally only adds data
    \item Three main states your files can be in:
    \begin{itemize}
      \item \textbf{Committed} - data is safely stored in your local database
      \item \textbf{Modified} - you have changed the file but have not committed
        it to the database yet
      \item \textbf{Staged} - you have marked a modified file in its current version
        to go into your next commit
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Git Basics}
  \centering
  \includegraphics[scale=0.4]{figures/git_sections.png}

  The tree main sections of a Git project
\end{frame}

\begin{frame}{Git Branching}
  \begin{block}{What does it mean?}
    Branching means you diverge from the main line of development and continue
    to do work without messing with that main line.
  \end{block}
  \centering
  \includegraphics[scale=0.33]{figures/branching.png}
\end{frame}

\begin{frame}{Creating a New Branch}
  \centering
  \includegraphics[scale=0.4]{figures/new_branch.png}

  Result of running \textbf{git checkout -b testing} on branch master
\end{frame}

\begin{frame}{Divergent History}
  \centering
  \includegraphics[scale=0.4]{figures/divergent_history.png}

  Result of working on separate branches simultaneously
\end{frame}

\begin{frame}{Simple Merging}
  \centering
  \includegraphics[scale=0.4]{figures/simple_merging.png}

  \textbf{master} is fast-forwarded to \textbf{hotfix}
\end{frame}

\begin{frame}{Three-way Merging}
  \centering
  \includegraphics[scale=0.35]{figures/three_way_merge.png}

  Merging \textbf{iss53} into \textbf{master} results in a merge commit
\end{frame}

\begin{frame}{Simple Rebase}
  \centering
  \includegraphics[scale=0.4]{figures/simple_rebase_example.png}

  Simple divergent history
\end{frame}

\begin{frame}{Simple Rebase}
  \centering
  \includegraphics[scale=0.37]{figures/simple_rebase.png}

  Rebasing the change introduced in \textbf{C4} onto \textbf{C3}
\end{frame}

\begin{frame}{Merge vs Rebase}
  \textbf{Pros}
  \begin{itemize}
    \item Merging is safe when working with others
    \item Rebasing keeps the commits tree flat
  \end{itemize}

  \textbf{Cons}
  \begin{itemize}
    \item Merging is not free of conflicts
    \item Merging will make the commits tree not readable by humans
    \item Merge commits are hard to revert
    \item Rebasing can cause conflicts when working with others
  \end{itemize}

  \begin{block}{Golden Rule of Rebasing}
    Never use it on \textit{public} branches
  \end{block}
\end{frame}

\begin{frame}{Main commands}
  \begin{itemize}
    \item \textbf{git init} - Create an empty Git repository or reinitialize an existing one
    \item \textbf{git clone} - Clone a repository into a new directory
    \item \textbf{git fetch} - Download objects and refs from another repository
    \item \textbf{git pull} - Fetch from and integrate with another repository or a local branch
    \item \textbf{git push} - Update remote refs along with associated objects
    \item \textbf{git add} - Add file contents to the index
    \item \textbf{git reset} - Reset current HEAD to the specified state
    \item \textbf{git checkout} - Switch branches or restore working tree files
    \item \textbf{git commit} - Record changes to the repository
  \end{itemize}
\end{frame}

\begin{frame}{Main commands}
  \begin{itemize}
    \item \textbf{git diff} - Show changes between commits, commit and working tree, etc
    \item \textbf{git status} - Show the working tree status
    \item \textbf{git show} - Show various types of objects
    \item \textbf{git log} - Show commit logs
    \item \textbf{git merge} - Join two or more development histories together
    \item \textbf{git rebase} - Reapply commits on top of another base tip
    \item \textbf{git branch} - List, create, or delete branches
    \item \textbf{git blame} - Show what revision and author last modified each line of a file
  \end{itemize}
\end{frame}

\begin{frame}{Day-to-day Git}
  When starting a new task:
  \begin{enumerate}
    \item git checkout master
    \item git fetch -p
    \item git pull origin master
    \item git checkout -b <my-branch>
    \item git add <changed-file1> <changed-file2> ...
    \item git commit
    \item Repeat 5 and 6 as many times as necessary
    \item git push -u origin <my-branch>
  \end{enumerate}
\end{frame}

\begin{frame}{Day-to-day Git}
  When continuing a task:
  \begin{enumerate}
    \item git checkout master
    \item git fetch -p
    \item git pull origin master
    \item git checkout <my-branch>
    \item git rebase master
    \item git push -f origin <my-branch>
    \item git add <changed-file1> <changed-file2> ...
    \item git commit
    \item Repeat 7 and 8 as many times as necessary
    \item git push origin <my-branch>
  \end{enumerate}
\end{frame}

\begin{frame}{Commit Guidelines}
  \begin{itemize}
    \item Remove trailing whitespaces
    \item Make each commit a logically separate changeset
    \item Write good commit messages
    \item \textbf{Signed-off-by: Another Dev <another\_dev@email.com>}
  \end{itemize}
  \begin{block}{Keep in mind}
    A well-crafted Git commit message is the best way to communicate context about a change to fellow developers (and indeed to their future selves). A diff will tell you what changed, but only the commit message can properly tell you why.
  \end{block}
\end{frame}

\begin{frame}{The Seven Rules of a Great Commit Message}
  \begin{enumerate}
    \item Separate subject from body with a blank line
    \item Limit the subject line to 50 characters
    \item Capitalize the subject line
    \item Do not end the subject line with a period
    \item Use the imperative mood in the subject line
    \item Wrap the body at 72 characters
    \item Use the body to explain what and why vs. how
  \end{enumerate}
\end{frame}

\begin{frame}{The Seven Rules of a Great Commit Message}
  \centering
  \includegraphics[scale=0.35]{figures/commit.png}
\end{frame}

\begin{frame}{Advanced Commands}
  \begin{itemize}
    \item \textbf{git stash} - Stash the changes in a dirty working directory away
    \item \textbf{git bisect} - Use binary search to find the commit that introduced a bug
    \item \textbf{git cherry-pick} - Apply the changes introduced by some existing commits
    \item \textbf{git rebase -i <branch>} - Make a list of the commits which are about to be rebased. Let the user edit that list before rebasing.
    \item \textbf{git revert} - Revert some existing commits
    \item \textbf{git rm} - Remove files from the working tree and from the index
    \item \textbf{git add -i} - Add modified contents in the working tree interactively to the index
    \item \textbf{git add -p} - Interactively choose hunks of patch between the index and the work tree and add them to the index
  \end{itemize}
\end{frame}

\begin{frame}{References}
  \begin{itemize}
    \item \textbf{Pro Git}, written by Scott Chacon and Ben Straub
      \url{https://git-scm.com/book/en/v2}
    \item \url{https://chris.beams.io/posts/git-commit/}
    \item \url{https://www.atlassian.com/git/tutorials/merging-vs-rebasing}
  \end{itemize}
\end{frame}

\end{document}
